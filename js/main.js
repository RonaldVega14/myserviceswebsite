
const $body = $('body');
const $menuOpen = $('.menu-toggle');
const $menuClose = $('.cancel-btn');
const $btnSearch = $('.search-toggle');
const htmlTag = document.querySelector('html');
const bodyTag = document.querySelector('body');
const navTag = document.querySelector('nav');
const navElementTag = document.querySelector('nav li a');

$(function () {
  $menuOpen.click(function () {
    $body.addClass('menu-open');
  });
  $menuClose.click(function () {
    $body.removeClass('menu-open');
  });

  $btnSearch.click(function () {
    $body.toggleClass('search-open');
  });

});


//Typed text

$(function () {
  $(".type-home").typed({
    strings: ['The Best Arts Online', 'Web Development', 'Mobile Development'],
    typeSpeed: 40,
    backSpeed: 20,
    backDelay: 1700,
    showCursor: true,
    loop: true
  });
});

// smooth scroll
$(document).ready(function () {
  // Add smooth scrolling to all links
  $("a").on('click', function (event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function () {

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

  $('.work-wrapper').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 2000,
    nextArrow: $('.next'),
    prevArrow: $('.prev'),
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          dots: true,
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });

});

//Active hover service
var serviceBx = document.querySelectorAll('.serviceBx');

$(function () {
  for (let i = 0; i < serviceBx.length; i++) {
    const element = serviceBx[i];
    element.addEventListener('mouseover', function () {
      element.className = 'serviceBx';
    });
    this.className = 'serviceBx active';
  }

})

//Navbar on scroll animation

let scrolled = () => {
  let dec = scrollY / (bodyTag.scrollHeight - innerHeight);
  console.log(Math.floor(dec * 100));
  return Math.floor(dec * 100);
}

addEventListener('scroll', () => {
  console.log('hola mundo');
  navTag.style.setProperty('background-color', scrolled() > 30 ? "rgba(31, 31, 37, .7)" : "rgba(31, 31, 37, .0)");
})
